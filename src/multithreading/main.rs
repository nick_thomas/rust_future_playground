/// https://nickymeuleman.netlify.app/blog/multithreading-rust#final-code
use std::{
    ascii::AsciiExt,
    collections::HashMap,
    mem,
    sync::{mpsc, Arc, Mutex},
    thread,
};

fn single_threade() {
    fn frequency(input: &[&str]) -> HashMap<char, usize> {
        let mut map = HashMap::new();

        for line in input {
            for c in line.chars().filter(|c| c.is_alphabetic()) {
                *map.entry(c.to_ascii_lowercase()).or_default() += 1;
            }
        }
        map
    }

    println!(
        "Output for count of hello: {:?}",
        frequency(&["hello", "world"])
    )
}

fn join_handled() {
    fn frequency(input: &[&str], worker_count: usize) -> HashMap<char, usize> {
        let mut result: HashMap<char, usize> = HashMap::new();
        let chunks = input.chunks((input.len() / worker_count).max(1));
        let mut handles = Vec::new();

        for chunk in chunks {
            let string = chunk.join("");

            let handle = thread::spawn(move || {
                let mut map: HashMap<char, usize> = HashMap::new();
                for c in string.chars().filter(|c| c.is_alphabetic()) {
                    *map.entry(c.to_ascii_lowercase()).or_default() += 1;
                }
                map
            });
            handles.push(handle);
        }

        for handle in handles {
            let map = handle.join().unwrap();
            for (key, value) in map {
                *result.entry(key).or_default() += value;
            }
        }
        result
    }
    println!(
        "Output for count of hello world join handle: {:?}",
        frequency(&["hello", "world"], 2)
    )
}

fn sender_receiver() {
    fn frequency(input: &[&str], worker_count: usize) -> HashMap<char, usize> {
        let mut result: HashMap<char, usize> = HashMap::new();
        let chunks = input.chunks((input.len() / worker_count).max(1));
        let (sender, receiver) = mpsc::channel();

        for chunk in chunks {
            let sender = sender.clone();
            let string = chunk.join("");
            thread::spawn(move || {
                let mut map: HashMap<char, usize> = HashMap::new();
                for c in string.chars().filter(|c| c.is_alphabetic()) {
                    *map.entry(c.to_ascii_lowercase()).or_default() += 1;
                }
                sender.send(map).unwrap();
            });
        }

        // drop the original sender, else the channel will remain open
        mem::drop(sender);

        for received in receiver {
            for (key, value) in received {
                *result.entry(key).or_default() += value;
            }
        }

        result
    }
    println!(
        "Output for count of hello world sender receiver: {:?}",
        frequency(&["hello", "world"], 2)
    )
}

fn mutex_example() {
    fn frequency(input: &[&str], worker_count: usize) -> HashMap<char, usize> {
        let mut result: HashMap<char, usize> = HashMap::new();
        let chunks = input.chunks((input.len() / worker_count).max(1));
        let mut handles: Vec<_> = Vec::new();

        for chunk in chunks {
            let string = chunk.join("");
            let handle = thread::spawn(move || {
                let mut map: HashMap<char, usize> = HashMap::new();

                for c in string.chars().filter(|c| c.is_alphabetic()) {
                    *map.entry(c.to_ascii_lowercase()).or_default() += 1;
                }
                map
            });
            handles.push(handle);
        }

        for handle in handles {
            let map = handle.join().unwrap();
            for (key, value) in map {
                *result.entry(key).or_default() += value;
            }
        }

        // get the inner value
        result
    }
    println!(
        "Output for count of hello world sender receiver: {:?}",
        frequency(&["hello", "world"], 2)
    )
}

fn main() {
    // single_threade()
    //join_handled()
    // sender_receiver()
    mutex_example()
}
