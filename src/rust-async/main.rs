use futures::stream::{self, StreamExt};
use futures::Future;
use lazy_static::lazy_static;
use rand::distributions::{Distribution, Uniform};
use std::time::Duration;
use tokio::time::{sleep, Instant};

lazy_static! {
    static ref START_TIME: Instant = Instant::now();
}

#[tokio::main]
async fn main() {
    // async example
    /*  let page = get_page(42).await;
    println!("Page #42: {:?}", page); */

    // Creating a stream
    // println!("First 10 pages:\n{:?}", get_n_pages(10).await);

    // Ordered  Streams
    // println!("First 10 pages, buffered by 5:\n{:?}",get_n_pages_buffered(10,5).await);

    // Unordered Streams
    // println!("First 10 pages, buffered by 5:\n{:?}",get_n_pages_buffer_unordered(10,5).await);

    // Flattening arrays
    /* println!(
        "IDS from first 5 pages:\n{:?}",
        get_ids_n_pages(5).collect::<Vec<_>>().await
    );
    println!(
        "IDs from first 5 pages, buffered by 3:\n{:?}",
        get_ids_n_pages_buffered(5, 3).collect::<Vec<_>>().await
    );
    println!(
        "IDs from first 5 pages, buffer-unordered by 3:\n{:?}",
        get_ids_n_pages_buffer_unordered(5, 3)
            .collect::<Vec<_>>()
            .await
    ); */

    println!(
        "IDs from first 25 items, buffered by 3 pages:\n{:?}",
        get_ids_n_items_buffered(25, 3).collect::<Vec<_>>().await
    )
}

// implements FnMut(usize) -> impl Future<Output = Vec<usize>>.
async fn get_page(i: usize) -> Vec<usize> {
    let millis = Uniform::from(0..10).sample(&mut rand::thread_rng());
    println!(
        "[{}] # get_page({}) will complete in {} ms",
        START_TIME.elapsed().as_millis(),
        i,
        millis
    );
    sleep(Duration::from_millis(millis)).await;
    println!(
        "[{}] # get_page({}) completed",
        START_TIME.elapsed().as_millis(),
        i
    );
    (10 * i..10 * (i + 1)).collect()
}

async fn get_n_pages(n: usize) -> Vec<Vec<usize>> {
    get_pages().take(n).collect().await
}

// then is used to obtain a stream of items which Vec<usize>
// we need to move otherwise i doesn't live long enough
fn get_pages() -> impl StreamExt<Item = Vec<usize>> {
    stream::iter(0..).then(|i| async move { get_page(i).await })
}

// Ordered buffering via buffered method
async fn get_n_pages_buffered(n: usize, buf_factor: usize) -> Vec<Vec<usize>> {
    get_pages_futures()
        .take(n)
        .buffered(buf_factor)
        .collect()
        .await
}

fn get_pages_futures() -> impl StreamExt<Item = impl Future<Output = Vec<usize>>> {
    stream::iter(0..).map(|i| get_page(i))
}

async fn get_n_pages_buffer_unordered(n: usize, buf_factor: usize) -> Vec<Vec<usize>> {
    get_pages_futures()
        .take(n)
        .buffer_unordered(buf_factor)
        .collect()
        .await
}

#[derive(Clone, Copy)]
struct Resource(usize);

impl std::fmt::Debug for Resource {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("r:{}", self.0))
    }
}

async fn fetch_resource(i: usize) -> Resource {
    let millis = Uniform::from(0..10).sample(&mut rand::thread_rng());
    println!(
        "[{}] ## fetch_resource({}) will complete in {} ms",
        START_TIME.elapsed().as_millis(),
        i,
        millis
    );
    sleep(Duration::from_millis(millis)).await;
    println!(
        "[{}] ## fetch_resource({}) completed",
        START_TIME.elapsed().as_millis(),
        i
    );
    Resource(i)
}

fn get_ids_n_pages(n: usize) -> impl StreamExt<Item = usize> {
    get_pages().take(n).flat_map(|page| stream::iter(page))
}

fn get_ids_n_pages_buffered(n: usize, buf_factor: usize) -> impl StreamExt<Item = usize> {
    get_pages_futures()
        .take(n)
        .buffered(buf_factor)
        .flat_map(|page| stream::iter(page))
}

fn get_ids_n_pages_buffer_unordered(n: usize, buf_factor: usize) -> impl StreamExt<Item = usize> {
    get_pages_futures()
        .take(n)
        .buffer_unordered(buf_factor)
        .flat_map(|page| stream::iter(page))
}

fn get_ids_n_items_buffered(n: usize, buf_factor: usize) -> impl StreamExt<Item = usize> {
    get_pages_futures()
        .buffered(buf_factor)
        .flat_map(|page| stream::iter(page))
        .take(n)
}

async fn collect_resources_n_pages(n: usize) -> Vec<Resource> {
    get_ids_n_pages(n)
        .then(|id| fetch_resource(id))
        .collect()
        .await
}
