use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};

/// https://dev.to/rogertorres/rust-tokio-stack-overview-runtime-9fh

#[tokio::main]
async fn main() {
    let db: Arc<Mutex<HashMap<&str, &str>>> = Default::default();
    tokio::spawn(foo(db.clone()));
    tokio::spawn(bar(db.clone()));
    handle(db).await;
    //   For concurrent example uncomment the below
    //   let (foo, bar) = tokio::join!{foo_concurrent(),bar_concurrent()};
    //    println!("{}{}",foo,bar);
}

async fn foo(db: Arc<Mutex<HashMap<&str, &str>>>) {
    let listener = std::net::TcpListener::bind("0.0.0.0:8080").unwrap();
    match listener.accept() {
        Ok(_pair) => loop {
            if let Ok(mut lock) = db.try_lock() {
                println!("`foo()` is finished");
                lock.insert("f", "foo");
                break;
            }
        },
        Err(_error) => println!("error"),
    }
}

async fn bar(db: Arc<Mutex<HashMap<&str, &str>>>) {
    let listener = std::net::TcpListener::bind("0.0.0.0:8081").unwrap();
    match listener.accept() {
        Ok(_pair) => loop {
            if let Ok(mut lock) = db.try_lock() {
                println!("`bar()` is finished");
                lock.insert("b", "bar");
                break;
            }
        },
        Err(_error) => println!("error"),
    }
}

async fn handle(db: Arc<Mutex<HashMap<&str, &str>>>) {
    loop {
        if let Ok(lock) = db.try_lock() {
            if lock.len() == 2 {
                println!("{}{}", lock.get("f").unwrap(), lock.get("b").unwrap());
                break;
            }
        }
    }
}

async fn foo_concurrent() -> &'static str {
    let listener = std::net::TcpListener::bind("0.0.0.0:8080").unwrap();
    match listener.accept() {
        Ok(_pair) => {
            println!("`foo()` is finished");
            "foo"
        }
        Err(_error) => "error",
    }
}

async fn bar_concurrent() -> &'static str {
    let listener = std::net::TcpListener::bind("0.0.0.0:8081").unwrap();
    match listener.accept() {
        Ok(_pair) => {
            println!("`bar()` is finished");
            "bar"
        }
        Err(_error) => "error",
    }
}
