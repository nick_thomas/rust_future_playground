use futures::executor::block_on;
use futures::Future;
use std::sync::{Arc, Mutex};

async fn second_greet() {
    println!("Good bye world");
}
async fn greeter() {
    println!("Hello World");
    // doesn't run directly, it is a dependency with second greet, it is subtask
}

async fn task2() -> u64 {
    println!("Second task");
    2
}

async fn task1() -> u64 {
    println!("First task");
    1
}
// Share state between threads
struct State {
    count: u64,
}
async fn state_task2(state: &Arc<Mutex<State>>) -> u64 {
    if let Ok(mut state) = state.lock() {
        state.count += 2;
    };
    2
}
async fn state_task1(state: &Arc<Mutex<State>>) -> u64 {
    if let Ok(mut state) = state.lock() {
        state.count += 3;
    };
    2
}

async fn async_main() {
    // greeter().await;
    // second_greet().await;

    // loads all functions at the same time and returns result in a tuple in the same order as input
    // task 1 and task 2 are not parallel, they are enqueued and run interleaved like Nodejs
    /*  let (result1, result2) = futures::join!(task1(), task2());
    dbg!(result1, result2); */

    // state sharing, cannot share state immutably across interleaving tasks
    // use arc/mutex to keep state only have single source of truth
    let state = State { count: 0 };
    let data = Arc::new(Mutex::new(state));
    futures::join!(state_task1(&data), state_task2(&data));
    if let Ok(s) = data.lock() {
        dbg!(&s.count);
    };
}
